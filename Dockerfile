FROM node:8

ENV NODE_ENV=production

RUN npm install -g swagger commander

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package.json  /usr/src/app/
RUN npm install

COPY . /usr/src/app

CMD swagger project start -m